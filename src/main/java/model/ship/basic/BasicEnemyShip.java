package model.ship.basic;

import model.ship.EnemyShip;

/**
 * Models a BasicSpaceShip belonging to an AI-driven opponent, with an associated
 * score value granted to the player when destroyed.
 */
public interface BasicEnemyShip extends BasicSpaceShip, EnemyShip { }
